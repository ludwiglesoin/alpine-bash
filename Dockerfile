FROM alpine:latest
MAINTAINER Ludwig LESOIN <llesoin@kiweesoft.com>

RUN apk add --no-cache \
  bash \
  coreutils
