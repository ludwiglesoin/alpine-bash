Alpine-bash
---

<img src="https://raw.githubusercontent.com/docker-library/docs/781049d54b1bd9b26d7e8ad384a92f7e0dcb0894/alpine/logo.png" width="100" height="100" />

<img src="https://camo.githubusercontent.com/7c9b27101ba491969d016f2f2427c3e066f7bd0b/68747470733a2f2f63646e2e7261776769742e636f6d2f6f64622f6f6666696369616c2d626173682d6c6f676f2f6d61737465722f6173736574732f4c6f676f732f4964656e746974792f504e472f424153485f6c6f676f2d7472616e73706172656e742d62672d636f6c6f722e706e67" width="200" height="100" />

<img src="https://avatars1.githubusercontent.com/u/4958508?s=200&v=4" width="100" height="100" />


This is a non offical Alpine Linux Docker image with:
---

* Bash Shell interpreter
* Gnu core utilities ([coreutils](https://en.wikipedia.org/wiki/GNU_Core_Utilities)). The coreutils permit to use all BASH subcommand as cp, chown, dd, truncate, etc

## Each version are automatically build every week with Gitlab-ci.


## Supported tags and gitlab source code 

| Version         |  Build Status  |  Description    |  Docker Hub |
|:----------------|:---------------|:----------------|:---------------|
| latest          | [![pipeline status](https://gitlab.com/ludwiglesoin/alpine-bash/badges/master/pipeline.svg)](https://gitlab.com/ludwiglesoin/alpine-bash/commits/master)| Latest version corresponding to the last tag (e.g: 3.10.1)  | [Docker Hub](https://cloud.docker.com/repository/docker/ludwiglesoin/alpine-bash/general)|
| 3.10    | [![pipeline status](https://gitlab.com/ludwiglesoin/alpine-bash/badges/3.10/pipeline.svg)](https://gitlab.com/ludwiglesoin/alpine-bash/commits/3.10) | Alpine 3.x Version | [Docker Hub](https://cloud.docker.com/repository/docker/ludwiglesoin/alpine-bash/general) |

How it work
---

### Download container and test


```
$ docker run --rm -it ludwiglesoin/alpine-bash
```

